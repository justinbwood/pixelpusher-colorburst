import com.heroicrobot.dropbit.registry.*;
import com.heroicrobot.dropbit.devices.pixelpusher.Pixel;
import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import java.util.*;

DeviceRegistry registry;

class TestObserver implements Observer {
  public boolean hasStrips = false;
  public void update(Observable registry, Object updatedDevice) {
    println("Registry changed!"); 
    if (updatedDevice != null) {
      println("Device change: " + updatedDevice);
    }
    this.hasStrips = true;
  }
}

TestObserver testObserver;

color[] pix;

void setup() {
  registry = new DeviceRegistry();
  testObserver = new TestObserver();
  registry.addObserver(testObserver);
  registry.setAntiLog(true);
  registry.setLogging(false);
  registry.setAutoThrottle(false);
  
  frameRate(30);
  size(528, 65);
  pix = new color[240];
}

void draw() {
  
  fill(0, 5);
  rect(0, 0, width, height);
    
  fill(random(255), random(255), random(255), 255);
  float diameter = random(50);
  ellipse(random(width), random(height), diameter, diameter);
  
  if (testObserver.hasStrips) {   
    registry.startPushing();
    List<Strip> strips = registry.getStrips();
    int numStrips = strips.size();
    if (numStrips == 0) return;
    
    for (Strip strip : strips) {
      
      int stripx = 0;
      for (int i = 55; i >= 0; i--) {
        PImage section = get(8*(i+4), 5, 8, 10);
        
        long avgRed = 0;
        long avgGreen = 0;
        long avgBlue = 0;
        long numPixels = section.pixels.length;
        for (int j = 0; j < numPixels; j++) {
          avgRed += red(section.pixels[j]);
          avgGreen += green(section.pixels[j]);
          avgBlue += blue(section.pixels[j]);
        }
        strip.setPixel(color(avgRed/numPixels, avgGreen/numPixels, avgBlue/numPixels), stripx);
        stripx++;
      }
      
      for (int i = 63; i >= 0; i--) {
        PImage section = get(8*(i+2), 20, 8, 10);
        
        long avgRed = 0;
        long avgGreen = 0;
        long avgBlue = 0;
        long numPixels = section.pixels.length;
        for (int j = 0; j < numPixels; j++) {
          avgRed += red(section.pixels[j]);
          avgGreen += green(section.pixels[j]);
          avgBlue += blue(section.pixels[j]);
        }
        strip.setPixel(color(avgRed/numPixels, avgGreen/numPixels, avgBlue/numPixels), stripx);
        stripx++;
      }
      
      for (int i = 63; i >= 0; i--) {
        PImage section = get(8*(i), 35, 8, 10);
        
        long avgRed = 0;
        long avgGreen = 0;
        long avgBlue = 0;
        long numPixels = section.pixels.length;
        for (int j = 0; j < numPixels; j++) {
          avgRed += red(section.pixels[j]);
          avgGreen += green(section.pixels[j]);
          avgBlue += blue(section.pixels[j]);
        }
        strip.setPixel(color(avgRed/numPixels, avgGreen/numPixels, avgBlue/numPixels), stripx);
        stripx++;
      }
      
      for (int i = 55; i >= 0; i--) {
        PImage section = get(8*(i+5), 50, 8, 10);
        
        long avgRed = 0;
        long avgGreen = 0;
        long avgBlue = 0;
        long numPixels = section.pixels.length;
        for (int j = 0; j < numPixels; j++) {
          avgRed += red(section.pixels[j]);
          avgGreen += green(section.pixels[j]);
          avgBlue += blue(section.pixels[j]);
        }
        strip.setPixel(color(avgRed/numPixels, avgGreen/numPixels, avgBlue/numPixels), stripx);
        stripx++;
      }
    }
    
    
  }
}